package com.example.app2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Spinner spinner1v;
    Button button1v;
    TextView inicio_label2v;
    String beer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spinner1v = (Spinner) findViewById(R.id.inicio_spinner1);
        button1v = (Button) findViewById(R.id.inicio_boton1);
        inicio_label2v = (TextView)findViewById(R.id.tinicio_label2);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this, R.array.beers_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1v.setAdapter(adapter);

        spinner1v.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                beer=adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void chooseBeer(View view){
        switch(beer){
            case "Rubia":
                beer ="Rubia fresquita que rica !!";
                break;
            case "Negra":
                beer ="Para cerveceros del norte !!";
            break;
            case "Tostada":
                beer ="Elaborada y con carácter !!";
                break;
            case "0.0":
                beer ="Cuidarse sin renunciar a una !!";
                break;
            default:
                Log.i("ERROR","NOT BEER");
                beer = "WATER";
        }
        Log.i("TEST",beer);
        inicio_label2v.setText(beer);
    }

}